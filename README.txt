**************************************************************
       'QUESTION AND ANSWER'  MODULE FOR DRUPAL 4.7.x
**************************************************************
Current Maintainer : Suuch (http://drupal.org/user/23178)
Contributors: disterics (http://drupal.org/user/48540)
Project Page: http://drupal.org/project/qanda
Documentation: http://drupal.org/node/?
Support: http://sbs.suuch.com/drupal/modules/qanda

Dependencies
Requires that Drupal 4.7.x is installed

Related Modules
TypeCheck Module for Drupal 4.7.x 
(http://drupal.org/project/typecheck)

FlashCard Module for Drupal 4.7.x 
(http://drupal.org/project/flashcard)
